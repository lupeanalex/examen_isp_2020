package lupean.alexandru.examen;

public class Ex1 {
    public interface I{ }
    public class B implements I{
        private String param;
        C c;
        Y y;
        B(){
            c=new C();
           }
    }
    public class Y{
        public void f(){}
    }
    public class Z{
        public void g(B b){}
    }
    public class E{
        B b;
    }
    public class C{}

}

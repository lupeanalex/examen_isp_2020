package lupean.alexandru.examen;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class GUI {
    private JFrame frame;
    private JTextField text1;
    private JTextField text2;
    private JButton but;
    private String toShow;

    public GUI(String toShow) {
        frame = new JFrame();
        text1 = new JTextField(50);
        text2 = new JTextField(50);
        but = new JButton("Click me!");
        this.toShow = toShow;

        initFrame();
        initText();
        frame.add(text1);
        frame.add(text2);
        frame.add(but);

        but.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (text1.getText().equals("") && text2.getText().equals("")) {
                    text1.setText(toShow);
                }
                else if (text1.getText().equals("")) {
                    text1.setText(text2.getText());
                    text2.setText("");
                }
                else {
                    text2.setText(text1.getText());
                    text1.setText("");
                }
            }
        });
    }

    private void initText() {
        text1.setEditable(true);
        text1.setEditable(true);
    }

    private void initFrame() {
        frame.setSize(640, 200);
        frame.setLayout(new FlowLayout());
        frame.setResizable(false);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public static void main(String[] args) {
        GUI gui = new GUI("text");
    }

}
